//
//  extensions.swift
//  20190516-NaveenSudireddy-NYCSchools
//
//  Created by Naveen Sudireddy on 5/16/19.
//  Copyright © 2019 Naveen Sudireddy. All rights reserved.
//

import Foundation
import UIKit


extension UIApplication {
    var topMostViewController : UIViewController? {
        return self.keyWindow?.rootViewController?.topMostViewController
    }
}
extension UIViewController {
    var topMostViewController : UIViewController {
        
        if let presented = self.presentedViewController {
            return presented.topMostViewController
        }
        
        if let navigation = self as? UINavigationController {
            return navigation.visibleViewController?.topMostViewController ?? navigation
        }
        
        if let tab = self as? UITabBarController {
            return tab.selectedViewController?.topMostViewController ?? tab
        }
        
        return self
    }
}
func showAlert(title: String, message: String) {
    DispatchQueue.main.async {
        ActivityIndicator.stopAnimation()
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        if let topViewControler = UIApplication.shared.topMostViewController {
            
            topViewControler.present(alert, animated: true,completion: nil)
            
        }
        
    }
    
}
