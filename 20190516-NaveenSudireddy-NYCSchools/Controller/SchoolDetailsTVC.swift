//
//  SchoolDetailsTVC.swift
//  20190516-NaveenSudireddy-NYCSchools
//
//  Created by Naveen Sudireddy on 5/16/19.
//  Copyright © 2019 Naveen Sudireddy. All rights reserved.
//

import UIKit
import MapKit

class SchoolDetailsTVC: UITableViewController {
    var schoolResults = [SatResultsData]()
    var schoolList = [SchoolsData]()
    var index = 0
    
    @IBOutlet weak var schoolNameCell: UITableViewCell!{
        didSet{
            schoolNameCell.textLabel?.numberOfLines = 0
            schoolNameCell.textLabel?.font = UIFont.boldSystemFont(ofSize: 14)
            schoolNameCell.textLabel?.textAlignment = .center
        }
    }
    @IBOutlet weak var overviewCell: UITableViewCell!{
        didSet{
            overviewCell.textLabel?.numberOfLines = 0
            overviewCell.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
        }
    }
    @IBOutlet weak var locationCell: UITableViewCell!{
        didSet{
            locationCell.textLabel?.numberOfLines = 0
            locationCell.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
            let tap = UITapGestureRecognizer.init(target: self, action: #selector(displaySchoolLocationOnMap))
            tap.numberOfTapsRequired = 1
            locationCell.addGestureRecognizer(tap)
            
        }
    }
    @IBOutlet weak var numberOfTestTakers: UILabel!
    @IBOutlet weak var mathsScore: UILabel!
    @IBOutlet weak var writingScore: UILabel!
    @IBOutlet weak var readingScore: UILabel!
    @IBOutlet weak var activitiesCell: UITableViewCell!{
        didSet{
            activitiesCell.textLabel?.numberOfLines = 0
            activitiesCell.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
        }
    }
    @IBOutlet weak var requirementsCell: UITableViewCell!{
        didSet{
            requirementsCell.textLabel?.numberOfLines = 0
            requirementsCell.textLabel?.font = UIFont.init(name: "Avenir", size: 14)
        }
    }



    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.largeTitleDisplayMode = .never
        tableView.rowHeight = UITableView.automaticDimension

    }
    override func viewWillAppear(_ animated: Bool) {
        
        let school = schoolList[index]
        schoolNameCell.textLabel?.text = school.schoolName
        
        overviewCell.textLabel?.text = school.overviewParagraph //About
        
        let location = String.init(format: "%@ \nEmail: %@ \nPhone: %@ \n%@",school.location ?? "", school.schoolEmail ?? "" , school.phoneNumber ?? "", school.website ?? "")
        locationCell.textLabel?.text = location
        
        activitiesCell.textLabel?.text = school.extracurricularActivities
        
        if let result = schoolResults.filter({$0.dbn == school.dbn}).first{
            numberOfTestTakers.text = result.numOfSatTestTakers ?? "NA"
            mathsScore.text = result.satMathAvgScore ?? "NA"
            writingScore.text = result.satWritingAvgScore ?? "NA"
            readingScore.text = result.satCriticalReadingAvgScore ?? "NA"
        }else{
            numberOfTestTakers.text = "NA"
            mathsScore.text = "NA"
            writingScore.text = "NA"
            readingScore.text = "NA"

        }
        var requirements = String.init(format: "%@ \n%@ \n%@ \n%@ \n%@",school.requirement11 ?? "", school.requirement21 ?? "" , school.requirement31 ?? "", school.requirement41 ?? "", school.requirement51 ?? "")
        requirements = requirements.trimmingCharacters(in: .whitespacesAndNewlines)
        requirementsCell.textLabel?.text = requirements == "" ? "NA" : requirements
        
      
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension //This Maintains Auto dimension WRT content
        
    }
    
    @objc func displaySchoolLocationOnMap()  {
        
        guard let latitude: CLLocationDegrees = Double(schoolList[index].latitude ?? "") else {return}
        guard let longitude: CLLocationDegrees = Double(schoolList[index].longitude ?? "") else {return}
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
//        mapItem.name = "Place Name"
        mapItem.openInMaps(launchOptions: nil)
        
    }
    

    
}
