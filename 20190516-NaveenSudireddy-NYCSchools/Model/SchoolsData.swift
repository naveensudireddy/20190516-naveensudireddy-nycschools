//
//  SchoolsData.swift
//  20190516-NaveenSudireddy-NYCSchools
//
//  Created by Naveen Sudireddy on 5/16/19.
//  Copyright © 2019 Naveen Sudireddy. All rights reserved.
//

import Foundation

struct SchoolsData : Codable {
    
    let academicopportunities1 : String?
    let academicopportunities2 : String?
    let academicopportunities3 : String?
    let addtlInfo1 : String?
    let admissionspriority11 : String?
    let admissionspriority21 : String?
    let admissionspriority31 : String?
    let attendanceRate : String?
    let bbl : String?
    let bin : String?
    let boro : String?
    let borough : String?
    let buildingCode : String?
    let bus : String?
    let censusTract : String?
    let city : String?
    let code1 : String?
    let communityBoard : String?
    let councilDistrict : String?
    let dbn : String?
    let directions1 : String?
    let eligibility1 : String?
    let ellPrograms : String?
    let extracurricularActivities : String?
    let faxNumber : String?
    let finalgrades : String?
    let grade9geapplicants1 : String?
    let grade9geapplicantsperseat1 : String?
    let grade9gefilledflag1 : String?
    let grade9swdapplicants1 : String?
    let grade9swdapplicantsperseat1 : String?
    let grade9swdfilledflag1 : String?
    let grades2018 : String?
    let interest1 : String?
    let languageClasses : String?
    let latitude : String?
    let location : String?
    let longitude : String?
    let method1 : String?
    let neighborhood : String?
    let nta : String?
    let offerRate1 : String?
    let overviewParagraph : String?
    let pctStuEnoughVariety : String?
    let pctStuSafe : String?
    let phoneNumber : String?
    let primaryAddressLine1 : String?
    let program1 : String?
    let requirement11 : String?
    let requirement21 : String?
    let requirement31 : String?
    let requirement41 : String?
    let requirement51 : String?
    let school10thSeats : String?
    let schoolAccessibilityDescription : String?
    let schoolEmail : String?
    let schoolName : String?
    let schoolSports : String?
    let seats101 : String?
    let seats9ge1 : String?
    let seats9swd1 : String?
    let stateCode : String?
    let subway : String?
    let totalStudents : String?
    let transfer : String?
    let website : String?
    let zip : String?
    
    enum CodingKeys: String, CodingKey {
        case academicopportunities1 = "academicopportunities1"
        case academicopportunities2 = "academicopportunities2"
        case academicopportunities3 = "academicopportunities3"
        case addtlInfo1 = "addtl_info1"
        case admissionspriority11 = "admissionspriority11"
        case admissionspriority21 = "admissionspriority21"
        case admissionspriority31 = "admissionspriority31"
        case attendanceRate = "attendance_rate"
        case bbl = "bbl"
        case bin = "bin"
        case boro = "boro"
        case borough = "borough"
        case buildingCode = "building_code"
        case bus = "bus"
        case censusTract = "census_tract"
        case city = "city"
        case code1 = "code1"
        case communityBoard = "community_board"
        case councilDistrict = "council_district"
        case dbn = "dbn"
        case directions1 = "directions1"
        case eligibility1 = "eligibility1"
        case ellPrograms = "ell_programs"
        case extracurricularActivities = "extracurricular_activities"
        case faxNumber = "fax_number"
        case finalgrades = "finalgrades"
        case grade9geapplicants1 = "grade9geapplicants1"
        case grade9geapplicantsperseat1 = "grade9geapplicantsperseat1"
        case grade9gefilledflag1 = "grade9gefilledflag1"
        case grade9swdapplicants1 = "grade9swdapplicants1"
        case grade9swdapplicantsperseat1 = "grade9swdapplicantsperseat1"
        case grade9swdfilledflag1 = "grade9swdfilledflag1"
        case grades2018 = "grades2018"
        case interest1 = "interest1"
        case languageClasses = "language_classes"
        case latitude = "latitude"
        case location = "location"
        case longitude = "longitude"
        case method1 = "method1"
        case neighborhood = "neighborhood"
        case nta = "nta"
        case offerRate1 = "offer_rate1"
        case overviewParagraph = "overview_paragraph"
        case pctStuEnoughVariety = "pct_stu_enough_variety"
        case pctStuSafe = "pct_stu_safe"
        case phoneNumber = "phone_number"
        case primaryAddressLine1 = "primary_address_line_1"
        case program1 = "program1"
        case requirement11 = "requirement1_1"
        case requirement21 = "requirement2_1"
        case requirement31 = "requirement3_1"
        case requirement41 = "requirement4_1"
        case requirement51 = "requirement5_1"
        case school10thSeats = "school_10th_seats"
        case schoolAccessibilityDescription = "school_accessibility_description"
        case schoolEmail = "school_email"
        case schoolName = "school_name"
        case schoolSports = "school_sports"
        case seats101 = "seats101"
        case seats9ge1 = "seats9ge1"
        case seats9swd1 = "seats9swd1"
        case stateCode = "state_code"
        case subway = "subway"
        case totalStudents = "total_students"
        case transfer = "transfer"
        case website = "website"
        case zip = "zip"
    }
}
