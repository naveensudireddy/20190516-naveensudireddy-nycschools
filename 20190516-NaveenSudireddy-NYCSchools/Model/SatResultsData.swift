//
//  SatResultsData.swift
//  20190516-NaveenSudireddy-NYCSchools
//
//  Created by Naveen Sudireddy on 5/16/19.
//  Copyright © 2019 Naveen Sudireddy. All rights reserved.
//

import Foundation

struct SatResultsData : Codable {
    
    let dbn : String?
    let numOfSatTestTakers : String?
    let satCriticalReadingAvgScore : String?
    let satMathAvgScore : String?
    let satWritingAvgScore : String?
    let schoolName : String?
    
    enum CodingKeys: String, CodingKey {
        case dbn = "dbn"
        case numOfSatTestTakers = "num_of_sat_test_takers"
        case satCriticalReadingAvgScore = "sat_critical_reading_avg_score"
        case satMathAvgScore = "sat_math_avg_score"
        case satWritingAvgScore = "sat_writing_avg_score"
        case schoolName = "school_name"
    }
}
