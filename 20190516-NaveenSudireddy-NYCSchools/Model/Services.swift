//
//  Services.swift
//  20190516-NaveenSudireddy-NYCSchools
//
//  Created by Naveen Sudireddy on 5/16/19.
//  Copyright © 2019 Naveen Sudireddy. All rights reserved.
//

import UIKit

class Services: NSObject {
    let NYSchoolListApi = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    let resultsApi = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    
    func getNYSchoolsData(completionHandler: @escaping ([SchoolsData]?) -> Void)  {
        guard let url = URL.init(string: NYSchoolListApi) else{return}
        
        let session = URLSession.shared.dataTask(with: URLRequest.init(url: url), completionHandler: {(data, response, error) -> Void in
            
            guard error == nil else{ showAlert(title: "Error getting data", message: (error?.localizedDescription)!)
                return}
            
            guard let httpResponse = response as? HTTPURLResponse,httpResponse.statusCode == 200 else{
                showAlert(title: "No response", message: "Please try again")
                return}
            
            guard let returnData = data else {
                showAlert(title: "No Data", message: "Please try again")
                return
            }
            do
            {
                let jsonDecoder = JSONDecoder()
                //Convert Json Data to model
                let responseModel = try jsonDecoder.decode([SchoolsData].self, from: returnData)
                /*  SchoolsData.saveData(data: responseModel)
                 print(responseModel) */                //  Can be used to maintain cache to have offline usage

                completionHandler(responseModel)
            }
            catch {
                print(error)
                showAlert(title: "Failed to get School List", message: error.localizedDescription)
            }
            
        })
        session.resume()
        
        
    }
    
    func getSATData(completionHandler: @escaping ([SatResultsData]?) -> Void)  {
        guard let url = URL.init(string: resultsApi) else{return}
        
        let session = URLSession.shared.dataTask(with: URLRequest.init(url: url), completionHandler: {(data, response, error) -> Void in
            
            guard error == nil else{ showAlert(title: "Error getting data", message: (error?.localizedDescription)!)
                return}
            
            guard let httpResponse = response as? HTTPURLResponse,httpResponse.statusCode == 200 else{
                showAlert(title: "No response", message: "Please try again")
                return}
            
            guard let returnData = data else {
                showAlert(title: "No Data", message: "Please try again")
                return
            }
            do
            {
                let jsonDecoder = JSONDecoder()
                let responseModel = try jsonDecoder.decode([SatResultsData].self, from: returnData)
                completionHandler(responseModel)
            }
            catch {
                print(error)
                showAlert(title: "Failed to get School results", message: error.localizedDescription)
            }        })
        session.resume()
        
    }
    
    
}

