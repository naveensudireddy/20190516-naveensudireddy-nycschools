//
//  TableViewTests.swift
//  20190516-NaveenSudireddy-NYCSchoolsTests
//
//  Created by Naveen Sudireddy on 5/18/19.
//  Copyright © 2019 Naveen Sudireddy. All rights reserved.
//

import XCTest
@testable import _0190516_NaveenSudireddy_NYCSchools

class TableViewTests: XCTestCase {

    var viewController: SchoolsListTVC!
    var numberOfRows: Int!
    var table: UITableView!
    
    override func setUp() {
        viewController = SchoolsListTVC()
        
        //Instantiate the view hierarchy
        XCTAssertNotNil(viewController.view)
        
        // can be used to test expected number of rows
        numberOfRows = viewController.tableView.numberOfRows(inSection: 0)
        
    }
    func testTableViewController_SetTableViewDataSource() {
        XCTAssertNotNil(viewController.tableView.dataSource)
    }
    
    func testTableViewController_SetTableViewDelegate() {
        XCTAssertNotNil(viewController.tableView.delegate)
    }
    
    func testTableViewController_ConformsToTableViewDataSourceProtocol() {
        XCTAssert(viewController.conforms(to: UITableViewDataSource.self))
        XCTAssert(viewController.responds(to: #selector(viewController.numberOfSections(in:))))
        XCTAssert(viewController.responds(to: #selector(viewController.tableView(_:numberOfRowsInSection:))))
        XCTAssert(viewController.responds(to: #selector(viewController.tableView(_:cellForRowAt:))))
    }
    
    func testTableViewController_ConformsToTableViewDelegateProtocol() {
        XCTAssert(viewController.conforms(to: UITableViewDelegate.self))
        XCTAssert(viewController.responds(to: #selector(viewController.tableView(_:didSelectRowAt:))))
    }
}
