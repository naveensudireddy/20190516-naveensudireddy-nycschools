//
//  _0190516_NaveenSudireddy_NYCSchoolsUITests.swift
//  20190516-NaveenSudireddy-NYCSchoolsUITests
//
//  Created by Naveen Sudireddy on 5/16/19.
//  Copyright © 2019 Naveen Sudireddy. All rights reserved.
//

import XCTest

class _0190516_NaveenSudireddy_NYCSchoolsUITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        
        let app = XCUIApplication()
        app.tables/*@START_MENU_TOKEN@*/.staticTexts["Clinton School Writers & Artists, M.S. 260"]/*[[".cells.staticTexts[\"Clinton School Writers & Artists, M.S. 260\"]",".staticTexts[\"Clinton School Writers & Artists, M.S. 260\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app.navigationBars["_0190516_NaveenSudireddy_NYCSchools.SchoolDetailsTVC"].buttons["Back"].tap()
                
    }

}
